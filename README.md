# Calculator App

This is a simple calculator application written in Java. It allows users to perform basic arithmetic operations and calculate the volume of a cylinder.

## Features

- Basic arithmetic operations: addition, subtraction, multiplication, division.
- Calculation of cylinder volume.
- Calculation of conus volume.
- Simple console-based user interface.


## Usage

1. Clone the repository to your local machine:

    ```bash
    git clone https://gitlab.com/VladyslavKotelevets/kotelevets_lab1.git
    ```

2. Compile the Java files:

    ```bash
    javac ./src/App.java
    ```

3. Run the application:

    ```bash
    java App
    ```

## Contributing

Contributions are welcome! If you find any bugs or have suggestions for improvement, please feel free to open an issue or create a pull request.

## Author

Vlad Kotelevets