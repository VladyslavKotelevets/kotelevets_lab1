import java.util.Scanner;

// Interface defining basic calculator operations
interface ICalculator {
    float sum(float x, float y);
    float diff(float x, float y);
    float mult(float x, float y);
    float div(float x, float y);
    void menu();
}

// Main class containing the application entry point
public class App {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.menu();
    }
}

// Calculator class implementing the ICalculator interface
class Calculator implements ICalculator {
    private Scanner scanner;

    public Calculator() {
        scanner = new Scanner(System.in);
    }

    // Method to display menu and handle user input
    @Override
    public void menu() {
        boolean exit = false;
        while (!exit) {
            System.out.println("Choose an operation:");
            System.out.println("1| Basic Calculation | + , - , / , * |");
            System.out.println("2| Calculate Cyl. volume");
            System.out.println("3| Calcuate Con. volume");
            System.out.println("4| Exit");

            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    handleArithmeticOperation(choice);
                    break;
                case 2:
                    System.out.println("Enter the radius:");
                    float radius = scanner.nextFloat();
                    System.out.println("Enter the height:");
                    float height = scanner.nextFloat();
                    System.out.println("Cylinder Volume: " + cylinderVolume(radius, height));
                    break;
                case 3:
                    System.out.println("Enter the radius:");
                    float radius_con = scanner.nextFloat();
                    System.out.println("Enter the height:");
                    float height_con = scanner.nextFloat();
                    System.out.println("Cylinder Volume: " + conusVolume(radius_con, height_con));
                    break;
                case 4:
                    exit = true; // Setting exit flag to true
                    System.out.println("Exiting...");
                    break;
                default:
                    System.out.println("Invalid choice!");
            }
        }
        scanner.close();
    }

    private void handleArithmeticOperation(int operation) {
        System.out.println("Enter number one: ");
        float num1 = scanner.nextFloat();
        System.out.println("Enter number two: ");
        float num2 = scanner.nextFloat();
        System.out.println("Choose an action by pressing 1-(+), 2-(-), 3-(*), 4-(/)");
        int choice = scanner.nextInt();

        // Performing selected operation
        switch (choice) {
            case 1:
                System.out.println("Result (Addition): " + sum(num1, num2));
                break;
            case 2:
                System.out.println("Result (Subtraction): " + diff(num1, num2));
                break;
            case 3:
                System.out.println("Result (Multiplication): " + mult(num1, num2));
                break;
            case 4:
                if (num2 == 0) {
                    System.out.println("Cannot divide by zero!");
                } else {
                    System.out.println("Result (Division): " + div(num1, num2));
                }
                break;
            default:
                System.out.println("Invalid operation!");
        }
    }

    // Cylinder function

    private float cylinderVolume(float radius, float height) {
        return (float) Math.PI * radius * radius * height;
    }
    private float conusVolume(float radius, float height) {
        return (1.0f/3.0f) * (float) Math.PI * radius * radius * height;
    }
    // Basic methods

    @Override
    public float sum(float x, float y) {
        return x + y;
    }

    @Override
    public float diff(float x, float y) {
        return x - y;
    }

    @Override
    public float mult(float x, float y) {
        return x * y;
    }

    @Override
    public float div(float x, float y) {
        return x / y;
    }
}